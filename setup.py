"""Setuptools configuration for txlocal."""

from setuptools import setup
from setuptools import find_packages


with open('README.rst', 'r') as readmefile:

    README = readmefile.read()

setup(
    name='txlocal',
    version='0.3.0',
    url='https://bitbucket.org/kconway/txlocal',
    description='Execution context locals for Twisted.',
    author="Kevin Conway",
    author_email="kconway@atlassian.com",
    long_description=README,
    license='Apache 2.0',
    packages=find_packages(exclude=['tests', 'build', 'dist', 'docs']),
    install_requires=[
        'twisted',
    ],
    extras_require={
        'testing': [
            'pep257',
            'pep8',
            'pyenchant',
            'pyflakes',
            'pylint',
            'pytest',
            'pytest-cov',
        ],
    },
    entry_points={
        'console_scripts': [

        ],
    },
    include_package_data=True,
)
