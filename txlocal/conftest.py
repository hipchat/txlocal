"""Pytest config."""

import pytest
import twisted.python.failure

from . import store as store_


@pytest.fixture(scope='session')
def wait_for():
    """Get a function that waits for deferreds to resolve."""
    def _wait_for(reactor, deferred):
        """Run the reactor until a deferred is resolved."""
        # Bork the cleaner to show the right stacktrace.
        twisted.python.failure.Failure.cleanFailure = lambda self: None
        state = {'complete': False}

        def resolve(value):
            """Flip the completed bit."""
            state['complete'] = value

        deferred.addBoth(resolve)
        while state['complete'] is False:

            reactor.iterate()

        if isinstance(state['complete'], twisted.python.failure.Failure):

            state['complete'].raiseException()

        return state['complete']

    return _wait_for


@pytest.fixture(scope='session')
def ctxfactory():
    """Get a context object factory."""
    return dict


@pytest.fixture(scope='function')
def ctxstore(ctxfactory):
    """Get a new ContextStore instance."""
    return store_.ContextStore(factory=ctxfactory)
