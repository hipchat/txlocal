"""Test suites for the context wrappers."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import functools
import weakref

import pytest

from . import wrappers


@pytest.fixture(scope='function')
def weakref_store(ctxstore):
    """Generate a context store that can be used for refcount tests."""
    class WeakRefableContext(object):

        """A context container that can be weakref'ed."""

    ctxstore = ctxstore.__class__(WeakRefableContext)
    return ctxstore


def test_contextmgr_success(ctxstore):
    """Check if the context manager swaps during success."""
    _, ctx1 = ctxstore()
    _, ctx2 = ctxstore()
    ctxstore['test_value'] = False
    with wrappers.context(ctxstore, ctx1):

        assert 'test_value' not in ctxstore

    assert ctxstore['test_value'] is False


def test_contextmgr_failure(ctxstore):
    """Check if the context manager swaps during failure."""
    _, ctx1 = ctxstore()
    _, ctx2 = ctxstore()
    ctxstore['test_value'] = False
    try:

        with wrappers.context(ctxstore, ctx1):

            ctxstore['test_value'] = True
            raise ValueError()

    except ValueError:

        pass

    assert ctxstore['test_value'] is False


def test_contextmgr_references(weakref_store):
    """Check that the context manager does not leak refcounts."""
    state = {"destroyed": False}

    def on_destroy(_):
        """Flip the test bit."""
        state['destroyed'] = True

    def scope_for_test():
        """Function scope that collapses to allow GC."""
        old, new = weakref_store()
        weak = weakref.ref(new, on_destroy)
        weakref_store(old)
        with wrappers.context(weakref_store, new):

            pass

        return weak

    weak = scope_for_test()  # Capture the weakref to persist the callback.
    assert state['destroyed'] is True
    del weak


def test_function_wrapper_references(weakref_store):
    """Check that the function wrapper does not leak refcounts."""
    state = {"wrapper": False, "ctx": False}

    def on_destroy(name, _):
        """Flip the test bit."""
        state[name] = True

    def do_nothing():
        """Do nothing."""
        pass

    def scope_for_test():
        """Function scope that collapses to allow GC."""
        old, new = weakref_store()
        weak_new = weakref.ref(new, functools.partial(on_destroy, 'ctx'))
        weakref_store(old)
        wrapped = wrappers.wrap_function(weakref_store, new, do_nothing)
        weak_wrap = weakref.ref(
            wrapped,
            functools.partial(on_destroy, 'wrapper'),
        )
        wrapped()
        return weak_new, weak_wrap

    # Capture the weakref to persist the callback.
    weak_new, weak_wrap = scope_for_test()
    assert state['wrapper'] is True
    assert state['ctx'] is True
    del weak_new
    del weak_wrap


def test_coro_swapping(ctxstore):
    """Check if the coroutine wrapper preserves context across calls."""
    def generator(value):
        ctxstore['test_value'] = value
        yield None
        assert ctxstore['test_value'] == value
        yield None
        assert ctxstore['test_value'] == value
        yield None
        assert ctxstore['test_value'] == value

    generator = wrappers.coroutine(ctxstore, generator)
    ctxstore()
    c1 = generator(1)
    ctxstore()
    c2 = generator(2)
    ctxstore()
    c3 = generator(3)

    for _ in xrange(3):

        c1.send(None)
        c2.send(None)
        c3.send(None)


def test_coro_failure(ctxstore):
    """Check if the coroutine wrapper handles exception values."""
    def generator(value):
        ctxstore['test_value'] = value
        yield None
        raise TypeError()

    generator = wrappers.coroutine(ctxstore, generator)
    ctxstore()
    c1 = generator(1)
    with pytest.raises(TypeError):

        c1.send(None)
        c1.send(None)


def test_coro_external_failure(ctxstore):
    """Check if the coroutine wrapper handles injected exception values."""
    def generator(value):
        ctxstore['test_value'] = value
        yield None

    generator = wrappers.coroutine(ctxstore, generator)
    ctxstore()
    c1 = generator(1)
    with pytest.raises(TypeError):

        c1.send(None)
        c1.throw(TypeError())


def test_coro_swapping_references(weakref_store):
    """Check that the function wrapper does not leak refcounts."""
    state = {"wrapper": False, 'ctx': False}

    def on_destroy(name, _):
        """Flip the test bit."""
        state[name] = True

    def do_nothing_coro():
        """Do nothing."""
        yield None
        yield None
        yield None

    def scope_for_test():
        """Function scope that collapses to allow GC."""
        old, new = weakref_store()
        weak_new = weakref.ref(new, functools.partial(on_destroy, 'ctx'))
        weakref_store(old)
        wrapped = wrappers.coroutine(weakref_store, do_nothing_coro)
        weak_wrap = weakref.ref(
            wrapped,
            functools.partial(on_destroy, 'wrapper'),
        )
        assert len(list(wrapped())) == 3
        return weak_new, weak_wrap

    # Capture the weakref to persist the callback.
    weak_new, weak_wrap = scope_for_test()
    assert state['wrapper'] is True
    assert state['ctx'] is True
    del weak_new
    del weak_wrap
