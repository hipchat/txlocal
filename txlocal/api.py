"""Easy use API for the project.

This module contains a function named `generate` that produces an
`ApiContainer` object. This is intended as a helper for creating instances of
the tools in this packages that are correctly bound to a ContextStore object.
The `ApiContainer` is a namedtuple that can be unpacked like this::

    import txlocal.api

    (
        store,
        context,
        wrap_function,
        wrap,
        coroutine,
        inlineCallbacks,
        ContextThreadPool,
        ContextReactor,
        install_reactor,
        install_threadpool,
        install_inline_callbacks,
    ) = txlocal.api.generate()

The values produced are:

    -   store

        A new ContextStore instance. The factory and validator for the store
        may be given as arguments to `generate`.

    -   context

        A context manager that accepts a context object and temporarily sets
        it as the active context. For example::

            old, new = store()
            with context(old):

                print(store.active_context)

            print(store.active_context)

    -   wrap_function

        A function wrapper that takes a context object and function. It returns
        a wrapped version of the function that will always execute with the
        given context object active. Example::

            def do():
                print(store.active_context)

            old, new = store()
            wrap_function(old, do)
            print(store.active_context)
            do()
            print(store.active_context)

    -   wrap

        A function decorator that wraps a function in the currently active
        context. Example::

        @wrap
        def do():
            print(store.active_context)

        old, new = store()
        print(store.active_context)
        do()
        print(store.active_context)

    -   coroutine

        A generator-coroutine function decorator that ensures the context is
        set at every step of the iteration.

    -   inlineCallbacks

        A drop-in replacement for the Twisted defer.inlineCallbacks that
        causes all portions of the contained generator-coroutine to execute
        within the same context in which the generator-coroutine was created.

    -   ContextThreadPool

        A twisted.python.threadpool.ThreadPool implementation that is bound to
        the provided ContextStore. It ensures threaded tasks are bound to the
        context in which they were created.

    -   ContextReactor

        A reactor implementation that manages context across networking events
        and callLater.

    -   install_reactor

        A function that, if executed, installs the given ContextReactor as the
        global reactor for the process. This must be called before any other
        code that uses the global reactor.

    -   install_threadpool

        A function that, if executed, installs the ContextThreadPool as the
        globally available implementation. Features that use threading, such
        as the ADBAPI, use the global ThreadPool implementation by default.
        This must be called before any other code that uses the global
        ThreadPool implementation.

    -   install_inline_callbacks

        A function that, if executed, installs the context aware
        inlineCallbacks decorator into the global twisted.internet.defer
        module. This must be called before any other code that uses
        inlineCallbacks is loaded.
"""

import collections
import functools

import twisted.internet.defer

from . import defer
from . import reactor as reactor_
from . import store as store_
from . import threadpool
from . import wrappers


ApiContainer = collections.namedtuple(
    'ApiContainer',
    (
        'store',
        'context',
        'wrap_function',
        'wrap',
        'coroutine',
        'inlineCallbacks',
        'ContextThreadPool',
        'ContextReactor',
        'install_reactor',
        'install_threadpool',
        'install_inline_callbacks',
    ),
)


def generate(factory=dict, validator=isinstance, reactor=None):
    """Generate an ApiContainer."""
    store = store_.ContextStore(factory=factory, validator=validator)
    context = functools.partial(wrappers.context, store)
    wrap_function = functools.partial(wrappers.wrap_function, store)

    def wrap(func):
        """Wrap a function in the current context."""
        return wrap_function(store.active_context, func)

    coroutine = functools.partial(wrappers.coroutine, store)
    inlineCallbacks = functools.partial(defer.inlineCallbacks, store)

    ContextThreadPool = threadpool.generate(store)
    install_threadpool = functools.partial(
        threadpool.install,
        ContextThreadPool,
    )

    ContextReactor = reactor_.generate(store, reactor, ContextThreadPool)
    install_reactor = functools.partial(reactor_.install, ContextReactor())

    def install_inline_callbacks():
        """Patch the global inlineCallbacks."""
        twisted.internet.defer.inlineCallbacks = inlineCallbacks

    return ApiContainer(
        store=store,
        context=context,
        wrap_function=wrap_function,
        wrap=wrap,
        coroutine=coroutine,
        inlineCallbacks=inlineCallbacks,
        ContextThreadPool=ContextThreadPool,
        ContextReactor=ContextReactor,
        install_reactor=install_reactor,
        install_threadpool=install_threadpool,
        install_inline_callbacks=install_inline_callbacks,
    )
