"""Test suites for the inlineCallbacks fork."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import functools
import random
import traceback

import pytest
from twisted.internet import reactor
import twisted.internet.defer
import twisted.python.failure

from . import defer


@pytest.fixture(scope='function')
def inlineCallbacks(ctxstore):
    return functools.partial(defer.inlineCallbacks, ctxstore)


def test_inlinecallbacks_sets_context(ctxstore, inlineCallbacks, wait_for):
    """Check if inlineCallbacks preserves context."""
    @inlineCallbacks
    def check(value):
        assert ctxstore['testvalue'] == value
        d = twisted.internet.defer.Deferred()
        reactor.callLater(random.random(), d.callback, None)
        print(value, ctxstore['testvalue'])
        yield d
        assert ctxstore['testvalue'] == value
        d = twisted.internet.defer.Deferred()
        reactor.callLater(random.random(), d.callback, None)
        print(value, ctxstore['testvalue'])
        yield d
        assert ctxstore['testvalue'] == value
        d = twisted.internet.defer.Deferred()
        reactor.callLater(random.random(), d.callback, None)
        print(value, ctxstore['testvalue'])
        yield d
        assert ctxstore['testvalue'] == value
        print(value, ctxstore['testvalue'])

    old, new = ctxstore()
    new['testvalue'] = 1
    test1 = check(1)
    ctxstore(old)

    old, new = ctxstore()
    new['testvalue'] = 2
    test2 = check(2)
    ctxstore(old)

    old, new = ctxstore()
    new['testvalue'] = 3
    test3 = check(3)
    ctxstore(old)

    d = twisted.internet.defer.DeferredList([test1, test2, test3])
    wait_for(reactor, d)
    # Push the current inlineCallbacks tick one ahead to do the final assert.
    # This is simulating what it would be at the end of an inlineCallbacks
    # chain but is complicated by the use of inlineCallbacks as a test wrapper.
    # The callback function that executes this iteration of the coroutine comes
    # from an unwrapped version of inlineCallbacks but executes within a
    # contexted inlineCallbacks. See the module docstring for the defer module
    # for more details.
    waiter = twisted.internet.defer.Deferred()
    reactor.callLater(0, waiter.callback, None)
    wait_for(reactor, waiter)
    assert ctxstore.active_context is None


def test_return_nonlocal_warns(inlineCallbacks, recwarn):
    """Ensure a warning is emitted when returnValue is used non-locally."""
    def inner_method():
        """Raise returnValue from the wrong place."""
        twisted.internet.defer.returnValue(1)

    @inlineCallbacks
    def coroutine():
        inner_method()
        twisted.internet.defer.returnValue(2)
        yield 0

    results = []
    deferred = coroutine()
    deferred.addCallback(results.append)

    assert results[0] == 1
    assert recwarn.list, "No DeprecationWarning was emitted."
    deprecation = recwarn.list[0]
    assert deprecation.category is DeprecationWarning
    assert str(deprecation.message) == (
        "returnValue() in 'inner_method' causing 'coroutine' to exit: "
        "returnValue should only be invoked by functions decorated with "
        "inlineCallbacks"
    )


def test_return_nonlocal_deferred(inlineCallbacks, recwarn):
    """Ensure warning is emitted for non-local returnValue after Deferred."""

    def inner_method():
        """Raise returnValue from the wrong place."""
        twisted.internet.defer.returnValue(1)

    test_deferred = twisted.internet.defer.Deferred()

    @inlineCallbacks
    def coroutine():
        yield test_deferred
        inner_method()
        twisted.internet.defer.returnValue(2)
        yield 0

    deferred = coroutine()
    results = []
    deferred.addCallback(results.append)
    assert not results
    test_deferred.callback(1)
    assert results[0] == 1
    assert recwarn.list, "No DeprecationWarning was emitted."
    deprecation = recwarn.list[0]
    assert deprecation.category is DeprecationWarning
    assert str(deprecation.message) == (
        "returnValue() in 'inner_method' causing 'coroutine' to exit: "
        "returnValue should only be invoked by functions decorated with "
        "inlineCallbacks"
    )


def test_yield_non_deferred(inlineCallbacks, wait_for):
    """Ensure yielding non-deferred values sends the same value back."""
    @inlineCallbacks
    def coroutine():
        value = yield 5
        twisted.internet.defer.returnValue(value)

    return_value = wait_for(reactor, coroutine())
    assert return_value == 5


def test_return_no_value(inlineCallbacks, wait_for):
    """Ensure a bare return results in a None value."""
    @inlineCallbacks
    def coroutine():
        yield 5
        return

    return_value = wait_for(reactor, coroutine())
    assert return_value is None


def test_return(inlineCallbacks, wait_for):
    """Ensure that returnValue resolves the Deferred with the right value."""
    @inlineCallbacks
    def coroutine():
        yield 5
        twisted.internet.defer.returnValue(6)

    return_value = wait_for(reactor, coroutine())
    assert return_value == 6


def test_non_generator(inlineCallbacks):
    """Ensure TypeError is raised when the function is not a coroutine."""
    @inlineCallbacks
    def notcoroutine():
        return 5

    with pytest.raises(TypeError):

        notcoroutine()


def test_non_generator_return(inlineCallbacks):
    """Ensure TypeError is raised when a non-coroutine uses returnValue."""
    @inlineCallbacks
    def notcoroutine():
        twisted.internet.defer.returnValue(5)

    with pytest.raises(TypeError):

        notcoroutine()


def success():
    """Return a Deferred that resolves to SUCCESS."""
    deferred = twisted.internet.defer.Deferred()
    reactor.callLater(0, deferred.callback, "SUCCESS")
    return deferred


def fail():
    """Return a Deferred that rejects with FAIL."""
    deferred = twisted.internet.defer.Deferred()

    def make_it_fail():
        deferred.errback(ZeroDivisionError('FAIL'))
    reactor.callLater(0, make_it_fail)
    return deferred


def basic():
    """Basic coroutine for Twisted test case ports."""
    success_value = yield success()
    assert success_value == "SUCCESS"
    try:
        yield fail()
    except ZeroDivisionError as exc:
        assert str(exc) == "FAIL"
    twisted.internet.defer.returnValue("BASIC")


def buggy():
    """Buggy coroutine for Twisted test case ports."""
    yield success()
    1/0


def test_basic(inlineCallbacks, wait_for):
    """Ensure the basic use case of inlineCallbacks works."""
    return_value = wait_for(reactor, inlineCallbacks(basic)())
    assert return_value == "BASIC"


def test_buggy(inlineCallbacks, wait_for):
    """Ensure basic failure handling works with inlineCallbacks."""
    with pytest.raises(ZeroDivisionError):

        # wait_for raises exceptions on Failure objects. Catching the exception
        # like this ensures inlineCallbacks errbacked with a Failure
        wait_for(reactor, inlineCallbacks(buggy)())


def test_nothing(inlineCallbacks, wait_for):
    """Ensure None is resolved when the generator falls off the end."""
    @inlineCallbacks
    def coroutine():
        if False:

            yield 1

    return_value = wait_for(reactor, coroutine())
    assert return_value is None


def test_tracebacks(inlineCallbacks, wait_for):
    """Ensure tracebacks are preserved by inlineCallbacks."""
    def get_division_failure():
        try:
            1/0
        except ZeroDivisionError:

            return twisted.python.failure.Failure()

    failure = get_division_failure()
    deferred = twisted.internet.defer.Deferred()
    try:
        failure.raiseException()
    except Exception:
        deferred.errback()

    results = []
    deferred.addErrback(results.append)
    assert results
    new_failure = results[0]
    tb = traceback.extract_tb(new_failure.getTracebackObject())

    assert len(tb) == 2
    assert 'test_defer' in tb[0][0]
    assert 'test_tracebacks' == tb[0][2]
    assert 'failure.raiseException()' == tb[0][3]
    assert 'test_defer' in tb[1][0]
    assert 'get_division_failure' == tb[1][2]
    assert '1/0' == tb[1][3]
