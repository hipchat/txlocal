# coding=utf-8

"""Threadpool extensions that maintain context.

This module extends default Twisted threadpool implementations to ensure that
all calls executed within non-reactor OS threads are done so within the context
under which they were deferred. The basic operations of the twisted threadpool
are this:

    -   Create a standing pool of N threads.
    -   Attach all threads to a threadsafe Queue data structure.
    -   When a call is deferrred, push a tuple of data to the Queue.
    -   A thread obtains the lock and pops the tuple of data.
    -   A thread uses the data tuple to handle executing the deferred function.

This module makes two major changes to the operation of the threadpool. First,
it adds an additional set of values to the data tuple: a context store and a
context object. Second, it modifies the thread worker body to wrap the deferred
function using the store and context objects. These, in conjunction with the
thread-safe internal storage used by the ContextStore, ensure that all deferred
function calls execute in their original context regardless of when the
function is executed by a thread, which thread executes the function, and how
many other threads are active and competing for CPU time.

All other code within the extended method bodies, with the exception of the
changes detailed above, is unmodified from the Twisted implementation. This
includes the docstring content for the modified methods. Adjustments to line
length and docstring structure have been altered to fit the PEP8 and PEP257
standards, but the behaviour of these methods is unchanged.
"""

###############################################################################
# License
###############################################################################
# This module contains substantial portions of the twisted.python.threadpool
# module from the Twisted project (https://twistedmatrix.com). What follows is
# the original copyright notice from that project taken at the time this
# work was created:
#
# Copyright (c) 2001-2015
# Allen Short
# Amber Hawkie Brown
# Andrew Bennetts
# Andy Gayton
# Antoine Pitrou
# Atlassian Pty Ltd
# Apple Computer, Inc.
# Ashwini Oruganti
# Benjamin Bruheim
# Bob Ippolito
# Canonical Limited
# Christopher Armstrong
# David Reid
# Divmod Inc.
# Donovan Preston
# Eric Mangold
# Eyal Lotem
# Google Inc.
# Hybrid Logic Ltd.
# Hynek Schlawack
# Itamar Turner-Trauring
# James Knight
# Jason A. Mobarak
# Jean-Paul Calderone
# Jessica McKellar
# Jonathan D. Simms
# Jonathan Jacobs
# Jonathan Lange
# Julian Berman
# Jürgen Hermann
# Kevin Horn
# Kevin Turner
# Laurens Van Houtven
# Mary Gardiner
# Massachusetts Institute of Technology
# Matthew Lefkowitz
# Moshe Zadka
# Paul Swartz
# Pavel Pergamenshchik
# Rackspace, US Inc.
# Ralph Meijer
# Richard Wall
# Sean Riley
# Software Freedom Conservancy
# Tavendo GmbH
# Thijs Triemstra
# Thomas Herve
# Timothy Allen
# Tom Prince
# Travis B. Hartwell
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
###############################################################################

import twisted
import twisted.python.context
import twisted.python.failure
import twisted.python.log
import twisted.python.threadpool

from . import wrappers


def generate_legacy(store):
    """Generate a threadpool using the legacy API."""
    class ContextThreadPool(twisted.python.threadpool.ThreadPool):

        def callInThreadWithCallback(self, onResult, func, *args, **kw):
            """Call a callable object in a separate thread.

            call C{onResult} with the return value, or a
            L{twisted.python.failure.Failure} if the callable raises an
            exception.

            The callable is allowed to block, but the C{onResult} function
            must not block and should perform as little work as possible.

            A typical action for C{onResult} for a threadpool used with a
            Twisted reactor would be to schedule a
            L{twisted.internet.defer.Deferred} to fire in the main
            reactor thread using C{.callFromThread}.  Note that C{onResult}
            is called inside the separate thread, not inside the reactor
            thread.

            @param onResult: a callable with the signature
                C{(success, result)}. If the callable returns normally,
                C{onResult} is called with C{(True, result)} where C{result} is
                the return value of the callable. If the callable throws an
                exception, C{onResult} is called with C{(False, failure)}.

                Optionally, C{onResult} may be C{None}, in which case it is not
                called at all.

            @param func: callable object to be called in separate thread

            @param *args: positional arguments to be passed to C{func}

            @param **kwargs: keyword arguments to be passed to C{func}
            """
            if self.joined:
                return
            ctx = twisted.python.context.theContextTracker.currentContext()
            ctx = ctx.contexts[-1]
            o = (ctx, func, args, kw, onResult, store, store.active_context)
            self.q.put(o)
            if self.started:
                self._startSomeWorkers()

        def _worker(self):
            """Method used as target of the created threads.

            Retrieve a task to run from the threadpool, run it, and proceed to
            the next task until threadpool is stopped.
            """
            ct = self.currentThread()
            o = self.q.get()
            while o is not twisted.python.threadpool.WorkerStop:
                with self._workerState(self.working, ct):
                    ctx, function, args, kwargs, onResult, store, current = o
                    del o
                    original, _ = store(current)
                    try:
                        result = twisted.python.context.call(
                            ctx,
                            function,
                            *args,
                            **kwargs
                        )
                        success = True
                    except:
                        success = False
                        if onResult is None:
                            twisted.python.context.call(
                                ctx,
                                twisted.python.log.err,
                            )
                            result = None
                        else:
                            result = twisted.python.failure.Failure()

                    del function, args, kwargs

                if onResult is not None:
                    if not hasattr(onResult, '__contexted__'):

                        onResult = wrappers.wrap_function(
                            store,
                            current,
                            onResult,
                        )
                    try:
                        twisted.python.context.call(
                            ctx,
                            onResult,
                            success,
                            result,
                        )
                    except:
                        twisted.python.context.call(
                            ctx,
                            twisted.python.log.err,
                        )

                del ctx, onResult, result

                store(original)
                with self._workerState(self.waiters, ct):
                    o = self.q.get()

            self.threads.remove(ct)

    return ContextThreadPool


def generate_modern(store):
    """Generate a threadpool implementation using the modern API."""
    class ContextThreadPool(twisted.python.threadpool.ThreadPool):

        def callInThreadWithCallback(self, onResult, func, *args, **kw):
            """Call a function in a thread.

            Call a callable object in a separate thread and call C{onResult}
            with the return value, or a L{twisted.python.failure.Failure} if
            the callable raises an exception.

            The callable is allowed to block, but the C{onResult} function must
            not block and should perform as little work as possible.

            A typical action for C{onResult} for a threadpool used with a
            Twisted reactor would be to schedule a
            L{twisted.internet.defer.Deferred} to fire in the main reactor
            thread using C{.callFromThread}.  Note that C{onResult} is called
            inside the separate thread, not inside the reactor thread.

            @param onResult: a callable with the signature
                C{(success, result)}. If the callable returns normally,
                C{onResult} is called with C{(True, result)} where C{result} is
                the return value of the callable.  If the callable throws an
                exception, C{onResult} is called with C{(False, failure)}.

                Optionally, C{onResult} may be C{None}, in which case it is not
                called at all.

            @param func: callable object to be called in separate thread

            @param args: positional arguments to be passed to C{func}

            @param kw: keyword arguments to be passed to C{func}
            """
            if self.joined:
                return
            ctx = twisted.python.context.theContextTracker.currentContext()
            ctx = ctx.contexts[-1]

            def inContext():
                """Run the target function in Twisted context.

                Note:
                    This is not the same as context local. This is related to
                    the Twisted concept of context which is bound by call
                    stack.
                """
                try:
                    result = inContext.theWork()
                    ok = True
                except:
                    result = twisted.python.failure.Failure()
                    ok = False

                inContext.theWork = None
                if inContext.onResult is not None:
                    inContext.onResult(ok, result)
                    inContext.onResult = None
                elif not ok:
                    twisted.python.log.err(result)

            func = wrappers.wrap_function(store, store.active_context, func)
            # Avoid closing over func, ctx, args, kw so that we can carefully
            # manage their lifecycle.  See
            # test_threadCreationArgumentsCallInThreadWithCallback.
            inContext.theWork = lambda: twisted.python.context.call(
                ctx,
                func,
                *args,
                **kw
            )
            inContext.onResult = onResult

            self._team.do(inContext)

    return ContextThreadPool


def generate(store):
    """Generate a threadpool implementation that binds context with a store.

    This function inspects the __version__ specifier of the currently installed
    Twisted implementation. If the installed Twisted is version 15 or above
    this function will return a threadpool that is compatible with the changes
    made in that version. Otherwise this function returns a version of the
    threadpool that is compatible with all versions below 15.
    """
    if int(twisted.__version__.split('.')[0]) >= 15:

        return generate_modern(store)

    return generate_legacy(store)


def install(threadpool):
    """Install a global ThreadPool implementation.

    This method takes a threadpool class and injects it into the global
    twisted.python.ThreadPool name. All imports _after_ this function will get
    a reference to the custom threadpool. All imports _before_ this function
    will maintain a reference to the old threadpool.

    This reference conflict is a natural aspect of the Python module system and
    the expected behaviour of import. It is critical that this function, if
    used, is called before _any_ other code that imports the default ThreadPool
    implementation. Failure to do so will result in undefined behaviour.
    """
    twisted.python.ThreadPool = threadpool
