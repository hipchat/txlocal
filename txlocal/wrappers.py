"""Tools for wrapping code in context.

This module contains a collection of helpers ranging from context managers to
metaclasses that are designed to help introduce context into code without the
need for large scale refactoring.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import contextlib
import functools
import sys

import twisted.internet.defer


_ORIGINAL_INLINE_CALLBACKS = twisted.internet.defer.inlineCallbacks


@contextlib.contextmanager
def context(store, ctx):
    """Temporarily set the currently active context.

    Args:
        store (ContextStore): An instance of the ContextStore interface.
        ctx: A valid context produced by the ContextStore.

    Raises:
        TypeError: If the given context is not valid.

    This context manager will temporarily set the currently active context to
    the given context. After the code within the `with` block executes the
    currently active context is reverted to what it was before the swap.
    """
    original, _ = store(ctx)
    try:

        yield store

    finally:

        store(original)


def wrap_function(store, ctx, func):
    """Wrap a function execution in a specific context.

    Args:
        store (ContextStore): An instance of the ContextStore interface.
        ctx: A valid context produced by the ContextStore.

    Returns:
        callable: A wrapped version of the function that manages swapping the
            requested context in and out.

    This function works exactly like a decorator only it takes more arguments
    than usual. The provided function is simply wrapped in a context switch.
    If possible, this function leverages `functools.wraps()` to ensure all the
    original function metadata are exposed appropriately. Due to the internals
    of the `functools.wraps()` helper some callables cannot have all of their
    metadata transferred. Some examples are properties, staticmethod, and
    partials which do not expose the __module__ attribute of the underlying
    function.

    Regardless `functools.wraps()` success, this function will always attach
    two attributes to the wrapped callable: `__contexted__` and `__wrapped__`.
    The `__contexted__` attribute is set to True and is intended to help in
    cases where code must know whether or not a function is context aware. The
    `__wrapped__` attribute contains a reference to the original callable for
    cases when unwrapping is required.

    Wrapping a function multiple times is not a valid use case. This function
    will allow, but the additional wraps will not overwrite the original
    context wrapping.
    """
    def wrapper(*args, **kwargs):
        """Set the context during the execution."""
        with context(store, ctx):

            return func(*args, **kwargs)

    try:

        wrapper = functools.wraps(func)(wrapper)

    except AttributeError:

        # Some things like staticfunction don't have all the attributes
        # needed to use functools wraps. Ignore the error and return the
        # wrapper without the original func metadata.
        pass

    wrapper.__contexted__ = True
    if not hasattr(wrapper, b'__wrapped__'):

        wrapper.__wrapped__ = func

    return wrapper


def coroutine(store, func):
    """Wrap a coroutine function in context.

    Args:
        store (ContextStore): An instance of the ContextStore interface.
        func (callable): Some callable that produces a generator-coroutine
            object.

    This wrapper consumes a generator-coroutine object and ensures that the
    context is set on each iteration. This function returned by this wrapper
    is, itself, a generator function that produces a generator-coroutine
    object so it is compatible with any Twisted components that consume such
    objects.

    When first executed, the returned wrapper will capture the currently active
    context object. That context will be set on every call to 'send()'. This
    allows coroutines to be paused and resumed without losing the active
    context.
    """
    @functools.wraps(func)
    def _outer(*args, **kwargs):
        coro = _wrapper(store.active_context, *args, **kwargs)
        coro.send(None)
        return coro

    @functools.wraps(func)
    def _wrapper(ctx, *args, **kwargs):
        coro = func(*args, **kwargs)
        # This early yield is used by the outer wrapper to prime the coroutine
        # before sending it out. This ensures the call to the wrapper is
        # proxied to the wrapped function immediately to prevent masking any
        # exceptions related to the call itself such as an incorrect number of
        # function arguments.
        yield None
        in_value = None
        out_value = None
        in_exc_info = None
        original_ctx = None
        while True:

            original_ctx, _ = store(ctx)

            try:

                if in_exc_info:

                    coro.throw(*in_exc_info)

                else:

                    out_value = coro.send(in_value)

            finally:

                store(original_ctx)

            try:

                in_value = yield out_value

            except:

                # Handle exceptions thrown into this coroutine.
                in_exc_info = sys.exc_info()

    return _outer
