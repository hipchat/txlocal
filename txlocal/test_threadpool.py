"""Test suites for the custom ThreadPool extensions."""

import functools

import pytest
import twisted.internet.defer
import twisted.internet.reactor

from . import threadpool


@pytest.yield_fixture(scope='function')
def tpool(ctxstore):
    pool = threadpool.generate(ctxstore)()
    pool.start()
    yield pool
    pool.stop()


def test_threadpool_preserves_context(ctxstore, tpool, wait_for):
    """Check if threads get the right context."""

    _, main_ctx = ctxstore()
    ctxstore['test_value'] = None

    def check_value(value):
        assert ctxstore['test_value'] == value

    def resolve_or_fail(deferred, success, value):
        if success:

            deferred.callback(value)
            return None

        deferred.errback(value)

    deferreds = []
    for count in xrange(20):

        ctxstore()
        ctxstore['test_value'] = count
        d = twisted.internet.defer.Deferred()
        tpool.callInThreadWithCallback(
            functools.partial(resolve_or_fail, d),
            functools.partial(check_value, count),
        )
        deferreds.append(d)

    wait_for(
        twisted.internet.reactor,
        twisted.internet.defer.DeferredList(deferreds),
    )

    ctxstore(main_ctx)
    assert ctxstore['test_value'] is None
