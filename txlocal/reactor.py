"""Twisted reactor extensions for context locals.

This module generates a new subclass of a given reactor that is bound to a
particular context store. Context binding is currently added to two primary
exit points of the reactor: callLater and the threadpool.

The callLater method is modified to automatically wrap any function given in
the the currently active context. This ensures the function is executed in the
same context as the callLater invocation regardless of when it actually
happens.

The internal threadpool of the reactor is replaced by the context bound
threadpool defined elsewhere in this package. This ensures any use of the
reactor to launch functions within separate OS threads are, like the targets
of callLater, bound to the same context in which the defer invocation is made.

Together these two additions capture all of the non-network bound exit points
of the reactor. Some items, such as the ADBAPI, use their own threadpool rather
than the one created by the reactor. These cases are not accounted for by this
reactor extension and must be accounted for in other ways like patching the
global threadpool implementation. See the threadpool module in this package
for more details. Additionally, network bound exit points must be accounted
for in a manner specific to the service. See the README and extended
documentation for more details on network bound entry and exit points.
"""

import sys

import twisted.internet.interfaces as txifaces

from . import wrappers
from . import threadpool as threadpool_


def generate(store, reactor=None, threadpool=None):
    """Generate a new reactor class from the given one.

    If the `reactor` argument is not given, or None, the default, global
    reactor will be used as the extension point.

    If the `reactor` argument is an instance of a reactor this function will
    extend the type from which the reactor was created.

    If the `reactor` argument is a reactor class it will be sublcasses directly
    and the subclass returned.

    The `threadpool` argument, if given, must be a constructor for a valid
    ThreadPool implementation.

    Raises:
        TypeError: If `reactor` is given but not a reactor.
    """
    if reactor is None:

        import twisted.internet.reactor
        reactor = twisted.internet.reactor

    reactor = type(reactor)

    if (
            not txifaces.IReactorCore.implementedBy(reactor) or
            not txifaces.IReactorThreads.implementedBy(reactor) or
            not txifaces.IReactorTime.implementedBy(reactor)
    ):

        raise TypeError('Can only extend reactors.')

    if not threadpool:

        threadpool = threadpool_.generate(store)

    class ContextReactor(reactor):

        """Reactor extension that adds context to async calls."""

        def __init__(self, *args, **kwargs):
            """Generate a new context object."""
            self.store = store
            reactor.__init__(self, *args, **kwargs)

        def callLater(self, _seconds, _f, *args, **kw):
            """Wrap callLater functions with the context."""
            _f = wrappers.wrap_function(store, store.active_context, _f)
            return reactor.callLater(self, _seconds, _f, *args, **kw)

        def _initThreadPool(self):
            """Create the threadpool accessible with callFromThread."""
            self.threadpool = threadpool(
                0, 10, 'twisted.internet.reactor')
            self._threadpoolStartupID = self.callWhenRunning(
                self.threadpool.start)
            self.threadpoolShutdownID = self.addSystemEventTrigger(
                'during', 'shutdown', self._stopThreadPool)

    return ContextReactor


def install(reactor):
    """Install a custom reactor.

    This method takes a reactor instance and injects it into the global
    twisted.internet.reactor name. All imports _after_ this function will get
    a reference to the custom reactor. All imports _before_ this function will
    maintain a reference to the old reactor.

    This reference conflict is a natural aspect of the Python module system and
    the expected behaviour of import. It is critical that this function, if
    used, is called before _any_ other code that imports the global reactor.
    Failure to do so will result in undefined behaviour.
    """
    del sys.modules['twisted.internet.reactor']
    sys.modules['twisted.internet.reactor'] = reactor
