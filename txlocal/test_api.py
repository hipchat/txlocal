"""Test suites for the API module."""

from . import api


def test_api_generates_unique_payloads():
    """Check that the API generator produces unique values when called."""
    api1 = api.generate()
    api2 = api.generate()
    assert api1.store is not api2.store
