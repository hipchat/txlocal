"""Test suites for the Twisted reactor extensions."""

import functools
import random

import pytest
import twisted.internet.defer

from . import reactor as reactor_
from . import defer as defer_


@pytest.fixture(scope='function')
def reactor(ctxstore):
    """Get a custom reactor bound to the test context store."""
    return reactor_.generate(ctxstore)()


@pytest.fixture(scope='function')
def inlineCallbacks(ctxstore):
    """Get an inlineCallbacks decorator bound to the test context store."""
    return functools.partial(defer_.inlineCallbacks, ctxstore)


def test_reactor_inlinecallbacks_sets_context(
        wait_for,
        ctxstore,
        reactor,
        inlineCallbacks,
):
    """Check if inlineCallbacks preserves context with a custom reactor."""
    original_ctx = ctxstore.active_context

    class CtxDaemon(object):

        def __init__(self, value):
            super(CtxDaemon, self).__init__()
            self.value = value
            original, self.ctx = ctxstore()
            ctxstore['testvalue'] = value
            ctxstore(original)

        @inlineCallbacks
        def check(self):
            ctxstore(self.ctx)
            assert ctxstore['testvalue'] == self.value
            d = twisted.internet.defer.Deferred()
            reactor.callLater(random.random(), d.callback, None)
            print(self.value, ctxstore['testvalue'])
            yield d
            assert ctxstore['testvalue'] == self.value
            d = twisted.internet.defer.Deferred()
            reactor.callLater(random.random(), d.callback, None)
            print(self.value, ctxstore['testvalue'])
            yield d
            assert ctxstore['testvalue'] == self.value
            d = twisted.internet.defer.Deferred()
            reactor.callLater(random.random(), d.callback, None)
            print(self.value, ctxstore['testvalue'])
            yield d
            assert ctxstore['testvalue'] == self.value
            print(self.value, ctxstore['testvalue'])

    test1 = CtxDaemon(1)
    test2 = CtxDaemon(2)
    test3 = CtxDaemon(3)
    wait_for(reactor, twisted.internet.defer.DeferredList([
        test1.check(),
        test2.check(),
        test3.check(),
    ]))
    assert ctxstore.active_context is original_ctx


def test_reactor_callLater_sets_context(ctxstore, reactor):
    """Check if callLater sets context for functions."""
    ctxstore()
    ctxstore['test_value'] = True

    def check_ctx():
        assert ctxstore['test_value'] is True

    d = reactor.callLater(0, check_ctx)
    ctxstore()
    ctxstore['test_value'] = False
    while d.active():

        reactor.iterate()
