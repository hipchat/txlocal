"""Test suites for the store module."""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import weakref

import pytest

from . import store


@pytest.fixture(scope='session')
def ctxfactory():
    """Get a context object factory."""
    class ContextTestFactory(object):

        """An empty context object for testing."""

    return ContextTestFactory


def test_active_none_if_not_in_context(ctxstore):
    """Check that active_context is None when not in a context."""
    assert ctxstore.active_context is None


def test_access_raises_if_not_active(ctxstore):
    """Check that NoActiveContext is raises if no context is active."""
    with pytest.raises(store.NoActiveContext):

        ctxstore.some_attribute


def test_call_no_args_generates(ctxstore):
    """Check that calling the store with not args generates a new context."""
    _, new = ctxstore()
    assert new is not None
    assert new is ctxstore.active_context


def test_call_with_args_sets(ctxfactory, ctxstore):
    """Check that calling the store with args sets the context."""
    sentinel = ctxfactory()
    _, new = ctxstore(sentinel)
    assert new is sentinel
    assert ctxstore.active_context is sentinel


def test_call_with_none_unsets(ctxstore):
    """Check that calling the store with None sets to no context."""
    ctxstore()
    assert ctxstore.active_context is not None
    ctxstore(None)
    assert ctxstore.active_context is None


def test_call_with_wrong_types_raises(ctxstore):
    """Check that setting to the wrong type raises TypeError."""
    with pytest.raises(TypeError):

        ctxstore(object())


def test_store_does_not_persist_references(ctxstore):
    """Check if the store is referencing removed contexts."""
    state = {"destroyed": False}

    def on_destroy(_):
        """Flip the test bit."""
        state['destroyed'] = True

    def scope_for_test():
        """Function scope that collapses to allow GC."""
        old, new = ctxstore()
        weak = weakref.ref(new, on_destroy)
        ctxstore(old)
        return weak

    weak = scope_for_test()  # Capture the weakref to persist the callback.
    assert state['destroyed'] is True
    del weak


def test_store_provides_item_management():
    """Check if the store supports __*item__ methods."""
    ctxstore = store.ContextStore()
    ctxstore()
    ctxstore['test_value'] = True
    assert ctxstore['test_value'] is True
    del ctxstore['test_value']
    assert 'test_value' not in ctxstore


def test_store_provides_attribute_management(ctxstore):
    """Check if the store supports __*attr__ methods."""
    _, ctx = ctxstore()
    ctxstore.test_value = True
    assert ctxstore.test_value is True
    assert hasattr(ctx, 'test_value')
    assert ctx.test_value is True
    del ctxstore.test_value
