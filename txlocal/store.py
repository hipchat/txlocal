"""A container for managing active execution contexts.

This module provides the basic data structure for managing context objects
and the state of which context is active. It is intended for use in a manner
similar to threadlocal and attempts to provide a, somewhat, similar interface.
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import threading


NOCONTEXT = object()


class NoActiveContext(Exception):

    """No context is active."""


class ContextStore(object):

    """A container for managing active execution contexts.

    Instances of this class are intended to be used in a manner similar to
    threadlocals and as singletons within a process. Multiple instances of this
    class can be created if multiple context containers are needed such as
    testing or developing multiple services within one code base that may, or
    may not, run within the same process space.

    Instances of this class expose a property called `active_context` that
    always resolves to the currently active context object. If no context is
    active this property resolves to None.

    Instances of this class are callable. Invocations of the __call__ method
    are the primary mechanism for altering the state of the store. All calls
    return a two-tuple of (<old context>, <new context>). Calling causes the
    following behaviours:

        -   old, new = store()  # No arguments

            Calling the store with no arguments causes the store to generated
            a new context object and set it as the active context. The `old`
            value is the context object that was previously active. The `new`
            value is the newly generated context object.

        -   old, new = store(None)  # Call with None

            Calling the store with an argument of None causes the store to
            unset any active context. The `old` value is the context object
            that was previously active and `new` is None.

        -   old, new = store(<some object>)  # Call with a value

            Calling the store with a value causes the store to adopt that value
            as the active context. The `old` value is the context object that
            was previously active. The `new` value is a reference to the object
            given during the call. If the given value is not valid a TypeError
            is raised.

    Other than the `active_context` property and callable usage, instances of
    this class proxy the currently active context. All magic methods are
    proxied to the active context and all attribute lookups (other than
    `active_context`) are sent to the context. If no context is active a
    NoActiveContext error is raised on usage of anything except
    `active_context` and the callable usage.

    All other tools in this package consume and interact with this interface.
    Additional implementations should behave similarly.
    """

    def __init__(self, factory=dict, validator=isinstance):
        """Initialize the store with a context factory.

        Args:
            factory (callable): A callable that accepts no arguments and
                produces a new context object. The default implementation
                is the built-in `dict` constructor.

            validator (callable): A callable that accepts a context object and
                context factory as positional arguments. It must return True
                or False. True indicates the value is a valid object produced
                by `factory`. False indicates the value is invalid. The default
                validator is the built-in `isinstance` function.
        """
        self.__factory = factory
        self.__validator = validator
        self.__active = threading.local()

    @property
    def active_context(self):
        """Get a reference to the active context.

        If not context is active this value resolved to None.
        """
        return getattr(self.__active, 'value', None)

    def __call__(self, ctx=NOCONTEXT):
        """Activate a new context.

        Args:
            ctx: A valid context object or None.

        Returns:
            tuple: A two tuple. The first value is the previously active
                context. The second value is the newly set context. Either or
                both may be None depending on the state of the store.

        Raises:
            TypeError: If the given object is not None and does not pass the
                validator given during the store's construction.

        If not `ctx` value is given this method will generate a new context
        object using the given factory and set it as the active context. See
        the class docstring for more details.
        """
        if (
                ctx is not NOCONTEXT and
                ctx is not None and
                not self.__validator(ctx, self.__factory)
        ):

            raise TypeError('Invalid context object: {0}'.format(ctx))

        # PERF: Bypass property lookup to avoid the descriptor overhead.
        original = getattr(self.__active, 'value', None)
        if ctx is NOCONTEXT:

            ctx = self.__factory()

        self.__active.value = ctx
        return (original, getattr(self.__active, 'value', None))

    def __getattr__(self, attr):
        """Proxy the active context object."""
        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return getattr(ctx, attr)

    def __setattr__(self, attr, value):
        """Proxy the active context object unless using internal attributes."""
        if attr in (
                '_ContextStore__factory',
                '_ContextStore__validator',
                '_ContextStore__active',
        ):

            return object.__setattr__(self, attr, value)

        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return setattr(ctx, attr, value)

    def __delattr__(self, attr):
        """Proxy the active context object unless using internal attributes."""
        if attr in (
                '_ContextStore__factory',
                '_ContextStore__validator',
                '_ContextStore__active',
        ):

            return object.__delattr__(self, attr)

        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return delattr(ctx, attr)

    def __getitem__(self, *args, **kwargs):  # pragma: no cover
        """Proxy the active context object."""
        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return ctx.__getitem__(*args, **kwargs)

    def __setitem__(self, *args, **kwargs):  # pragma: no cover
        """Proxy the active context object."""
        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return ctx.__setitem__(*args, **kwargs)

    def __delitem__(self, *args, **kwargs):  # pragma: no cover
        """Proxy the active context object."""
        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return ctx.__delitem__(*args, **kwargs)

    def __contains__(self, *args, **kwargs):  # pragma: no cover
        """Proxy the active context object."""
        ctx = getattr(self.__active, 'value', None)
        if ctx is None:

            raise NoActiveContext()

        return ctx.__contains__(*args, **kwargs)
