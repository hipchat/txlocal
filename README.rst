========
txlocal
========

*Threadlocal like object support for Twisted.*

About
=====

This project adds a threadlocal like object to the Twisted environment.

One of the challenges related to managing concurrency with Twisted is the lack
of a thread, green-thread, or thread-like concept. From the reactor's point of
view there are just some number of sockets waiting on read/write events. When
one of those events is received from the underlying polling mechanism the
reactor executes some callback that does a unit of work until it needs to wait
for another event.

While Twisted may have no concept of a thread, or execution context, services
tend to have clearly defined boundaries for individual requests. In an HTTP
service, for example, everything that occurs within a single TCP connection
represents one request, or one transaction. In streaming services, like
XMLStream, each element received, and the processing of that element, can be
considered a single request, or transaction. As developers we can logically
define these related code paths even if Twisted does not provide for knowing
which transaction is active at any given time.

This library provides a tool-kit for connecting code paths that would otherwise
be separated by network events, timer events, or threading events. It does this
by provided a threadlocal like object and a suite of Twisted extensions that
mimic the inter-connected nature of threaded code.

How To Use
==========

Generate An API
---------------

First, generate an instance of the API for your project. This can be a new
module called `context.py` with the following contents:

.. code-block:: python

    import txlocal.api

    (
        store,
        context,
        wrap_function,
        wrap,
        coroutine,
        inlineCallbacks,
        ContextThreadPool,
        ContextReactor,
        install_reactor,
        install_threadpool,
        install_inline_callbacks,
    ) = txlocal.api.generate()

This will generate a series of objects and functions that add context local
storage to a Twisted service.

Install The Globals
-------------------

The next step is installing the context aware versions of the Twisted Reactor
and ThreadPool. To do this you must find a place in your code where you can
import and run the installation functions generated in the previous step. The
installation *must* happen before any other Twisted code is imported.

.. code-block:: python

    from myproject import context
    context.install_threadpool()
    context.install_reactor()
    context.install_inline_callbacks()

    # All other code comes after.

This will do three things. First it replaces the standard implementation of
ThreadPool in favour of one that is context aware. Some features, such as the
ADBAPI, pull directly from the global ThreadPool. Installing the context aware
version allows all code using the global ThreadPool to accurately maintain
context across threads.

After installing the threadpool this code will install a new global Reactor
implementation that is also context aware. It ensures that  usage of callLater
accurately maintain context during asynchronous events.

The last thing it does is install the new context aware inlineCallbacks
decorator. This decorator replaces the default implementation of
inlineCallbacks to ensure it accurately maintains context across multiple
pauses and resumes. None of the core Twisted code uses this decorator, but it
must be installed before any third party code that does use it.

Initialising Context
--------------------

With the context aware extensions installed, the final piece of setup is
determining where to create the context object for a particular transaction.
This initial create point is heavily dependent on the type of service and the
intended usage of the context local storage. The key factor is to identify a
point in code where a logical sequence of events begins.

Lower Level Protocols
^^^^^^^^^^^^^^^^^^^^^

In a Twisted service, every connection results in the creation of a Protocol
instance. Typically, this Protocol is responsible for all of the application
level logic. One reasonable start point for a new context is the `__init__`
method of a Protocol class:

.. code-block:: python

    from twisted.internet.protocol import Protocol
    from myproject.ctx import store, context

    class MyProtocol(Protocol):

        def __init__(self):
            old, self._ctx = store()
            store(old)  # Always reset to avoid stale context.

        def doSomething(self):
            with context(self._ctx):
                # Perform application logic.

HTTP Services
^^^^^^^^^^^^^

Sometimes the Protocol class is too far down the abstraction chain to be an
easy choice for context generation. If working directly with the Twisted `web`
module, for example, the entry point for application code is pushed up to
implementations of `IResource`. One possibility to to create a very thin
subclass of the `Request` and `Site` objects:

.. code-block:: python

    from twisted.web import server
    from myproject.ctx import store, context

    class CtxRequest(server.Request):
        def __init__(self, *args, **kwargs):
            server.Request.__init__(self, *args, **kwargs)
            old, self._ctx = store()
            store(old)

        def process(self):
            with context(self._ctx):
                return server.Request.process(self)


    class CtxSite(server.Site):
        requestFactory = CtxRequest

The new `Site` subclass can be used anywhere the original would but now ensures
that each new HTTP request generates a new context object and activates that
context before calling into application logic.

Maintaining Context
-------------------

All application code in Twisted has to break somewhere for an asynchronous
event. Execution breaks for one of three reasons: 1) network/io events,
2) time based events, and 3) threaded events. When execution breaks, the
Twisted reactor will start handling events that will, most likely, activate
code that is attached to an entirely different logical "thread" of execution.
In order to preserve the correct context from break to break, code must be
bound to a context before it leaves the blocking, sequential execution.

callLater
^^^^^^^^^

One of the common breaks in Twisted is the use of `reactor.callLater`. Any
functions fed to `callLater` need to be wrapped in the correct context. The
reactor extension provided by this package does this automatically by wrapping
all functions passed to `callLater` with the context that was active at the
time. In lieu of those extensions, the same process can be done explicitly by
using the `wrap` decorator from the generated API:

.. code-block:: python

    from myproject.ctx import wrap
    from twisted.internet import reactor

    def doLater():
        print('I happen later.')

    reactor.callLater(0, wrap(doLater))

threads
^^^^^^^

Another common breakpoint, because of its use in the ADBAPI, is the use of
`threadpool.deferToThread` and `reactor.callInThread`. The threadpool
extensions provided by this package automatically bind the function deferred to
a thread in the currently active context just like `callLater`. Without the
extensions the binding can be performed with the `wrap` decorator in a manner
similar to `callLater`.

.. code-block:: python

    from myproject.ctx import wrap
    from twisted.internet import reactor

    def doInThread():
        print('I am in a thread.')

    reactor.callInThread(wrap(doInThread))

Deferred
^^^^^^^^

A common output of functions in the higher levels of Twisted abstractions is a
`Deferred` object. There are no Deferred extensions provided by this package so
all callback and errback functions must be bound to a context explicitly:

.. code-block:: python

    from myproject.ctx import wrap
    from twisted.internet import reactor
    from twisted.internet.defer import Deferred

    def getDeferred():
        d = Deferred()
        reactor.callLater(0, d.callback, None)
        return d

    def cb(_):
        print('I am a callback.')

    getDeferred().addCallback(wrap(cb))

inlineCallbacks
^^^^^^^^^^^^^^^

The `inlineCallbacks` generator-coroutine scheduler provided by Twisted is a
complicated case for explicit context binding. The suggested way of performing
the correct binding is by installing the `inlineCallbacks` extension provided
by this package. The extension will implicitly bind to the currently active
context and ensure that each pump of the wrapped generator-coroutine is
performed within the right context:

.. code-block:: python

    from myproject.ctx import install_inline_callbacks, wraps
    from twisted.internet import reactor
    from twisted.internet.defer import inlineCallbacks, Deferred

    @inlineCallbacks
    def someTask():
        d = Deferred()
        reactor.callLater(0, d.callback, None)
        yield d
        d = Deferred()
        reactor.callLater(0, d.callback, None)
        yield d

    def cb(_):
        print('I am a callback.')

    taskDeferred = someTask()
    taskDeferred.addCallback(wrap(cb))

Performance Notes
=================

We've taken care to keep the cost of managing and switching contexts around
low. However, there *is* a cost. Here are some suggestions for keeping the
CPU and memory cost of context locals low:

Don't Switch In The Core
------------------------

One mistake we made early on was trying to put the switching of context objects
in some of the core networking code paths. For example, we tried putting a
switch inside the Transport layer to ensure the right context was active during
send and receive data events:

.. code-block:: python

    # NOTE: Don't do this.

    from myproject.ctx import store, context
    import twisted.application.internet
    import twisted.internet.tcp

    class Server(twisted.internet.tcp.Server, object):

        """TCP transport extension that acts as a root context."""

        def __init__(self, *args, **kwargs):
            """Create a new context for the connection."""
            old, new = store()
            twisted.internet.tcp.Server.__init__(self, *args, **kwargs)
            store(old)
            self._context = new

        def doRead(self, *args, **kwargs):
            """Set the context for the reader callbacks."""
            with context(self._context):
                twisted.internet.tcp.Server.doRead(self, *args, **kwargs)

        def doWrite(self, *args, **kwargs):
            """Set the context for writer callbacks."""
            with context(self._context):
                twisted.internet.tcp.Server.doWrite(self, *args, **kwargs)

    class TcpPort(twisted.internet.tcp.Port, object):

        """TCP transport factory extension."""

        transport = Server

        def __init__(self, *args, **kwargs):
            twisted.internet.tcp.Port.__init__(self, *args, **kwargs)

    class TCPServer(twisted.application.internet.TCPServer):

        """TCPServer extension that uses context transports."""

        def __init__(self, *args, **kwargs):
            twisted.application.internet.TCPServer.__init__(
                self,
                *args,
                **kwargs
            )

        def _getPort(self):
            """Generate a context bound TCP socket."""
            _port = TcpPort(*self.args, **self.kwargs)
            _port.startListening()
            return _port

The above code, if used, creates a new context local for every incoming TCP
connection and ensure that context is active every time bytes are read from and
written to that connection. The result is a clean and easy context management
that guarantees context during all Protocol code execution. However, the cost
of switching on network events is high. This solution may work well for low
throughput services, but switching on this frequency does not scale well and
results in extremely high CPU usage. The suggestion is to choose a lower
frequency event, such as `lineReceived` or `onElement` which occur after
multiple reads and data parsing.

Use Slots For Context Objects
-----------------------------

The default factory for context objects generates a dictionary. The memory
allocation behaviour of dictionaries makes them less than ideal for generation
at a high rate when there are more than a handful of attributes to store. The
suggestion is to use a slotted class definition to keep allocation time and
memory consumption low:

.. code-block:: python

    class CustomContext(object):
        __slots__ = ('request_id', 'user_id', 'account_id')

For more reading on slots see the
`Python Data Model <https://docs.python.org/2/reference/datamodel.html#slots>`_.
For more reading on the memory allocation behaviour of dictionaries see the
`cPython dictionary notes <https://github.com/python/cpython/blob/master/Objects/dictnotes.txt>`_.

If you have a general idea of what data are needed for the context local object
you can run variations of the following timers to see if there is any
difference between the implementations:

.. code-block:: shell

    python -m timeit -s 'class CustomContext(object):' -s '    __slots__ = ("a", "b", "c", "d", "e", "f", "g", "h", "i")' -- 'CustomContext()'

    python -m timeit -- '{"a": None, "b": None, "c": None, "d": None, "e": None, "f": None, "g": None, "h": None, "i": None}'

Bypass The Context Manager
--------------------------

The API generator creates a context manager, called `context` in the docs, that
accepts a context object and sets it as active for the duration of the `with`
body where it is used. On exit it resets the state of the store to what it was
before the manager was used. In very tight loops and high throughput services
the added cost of calling `__enter__` and `__exit__` can add additional runtime
and CPU burn. To reduce the impact in systems sensitive to this added cost the
context manager can be unravelled:

.. code-block:: python

    from myproject.ctx import store

    def doSomething():
        old, new = store()
        try:
            # Perform logic.
        finally:
            store(old)

This strategy adds some verbosity and boilerplate to the code, but is effective
if absolutely needed.

API Customisation
=================

The API generator accepts a few arguments that provide for customisation of
the generated output.

Custom Context Objects
----------------------

The `ContextStore` created by the API generator defaults to using a dictionary
as the context object. Custom context objects may be defined and bound to the
`ContextStore` at generation time.

.. code-block:: python

    import txlocal.api

    class LightweightContext(object):

        __slots__ = ('request_id', 'user_id')

    txlocal.api.generate(factory=LightweightContext)

The factory parameter may be any callable that requires no arguments.

Custom Context Validators
-------------------------

As a safety feature, the `ContextStore` does not set the active context to
anything that fails its validation function. The default validator is the
`isinstance()` function. Custom validators may be given as any callable that
accepts two arguments: the proposed context object and the given factory
callable.

.. code-block:: python

    import txlocal.api

    # Disable validation by always returning true.
    txlocal.api.generate(validator=lambda context, factory: True)


Custom Reactors
---------------

The default behaviour of the API generator is to use the default, global
Reactor implementation as a base. However, the API generator also accepts a
specific Reactor implementation to subclass.

.. code-block:: python

    import txlocal.api
    import twisted.internet.epollreactor

    txlocal.api.generate(reactor=twisted.internet.epollreactor.EPollReactor)

Testing
=======

All tests are written with pytest and run against Python2.6 and Python2.7. In
addition there are style gates using the PEP8, PEP257, and PyFlakes linters.
All tests are immediately adjacent the modules they test.

License
=======

    Copyright 2015 Atlassian Pty Ltd

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.


Contributing
============

Style
-----

As long as the code passes the PEP8 and PyFlakes gates then the style is
acceptable.

Docs
----

The PEP257 gate will check that all public methods have docstrings. This
project uses the
`napoleon style of docstrings <https://pypi.python.org/pypi/sphinxcontrib-napoleon>`_
for documenting functions and classes. The only exceptions to this are
docstrings that have been preserved from the original Twisted code base such as
those in the threadpool module.

Tests
-----

Make sure the patch passes all the existing tests. If you're adding a new
feature don't forget to throw in a test or two. If you're fixing a bug then add
at least one test to prevent regressions.

Contributing Agreement
----------------------

Atlassian requires that you fill out a contributor's agreement before we can
accept your patch. If you are an individual you can fill out the
`individual CLA <https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d>`_.
If you are contributing on behalf of your company then fill out the
`corporate CLA <https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b>`_.
